import itertools
from jinja2 import Template
import copy
import time


class Cell:
    def __init__(self, value, r_sum=0, d_sum=0):
        self.value = value
        self.r_sum = r_sum
        self.d_sum = d_sum

        self.constraints = []

        if self.value == 0:  # is a free cell
            self.possible_values = {1, 2, 3, 4, 5, 6, 7, 8, 9}
        else:
            self.possible_values = {self.value}

    def add_constraint(self, cst):
        self.constraints.append(cst)

    def intersect(self, values):
        tmp = self.possible_values & values

        # if self.y == 6 and self.x == 4:
        #     print(self.possible_values, values, self.value, tmp, self.constraints)

        if len(tmp) == 0:  # no possible values
            return True, False

        if len(tmp) == 1:  # only one possible value
            self.value = next(iter(tmp))
            self.possible_values = tmp
            return True, True

        if len(tmp) == len(self.possible_values):  # nothing changed
            return False, True

        self.possible_values = tmp

        return True, True

    def set(self, v):
        self.value = v
        self.possible_values = {v}

    @staticmethod
    def from_string(txt):
        if "\\" in txt:
            d, r = txt.split("\\")
            return Cell(-1, int(r), int(d))

        return Cell(int(txt))


class SumConstraint:
    def __init__(self, sum):
        self.initial_value = sum
        self.sum = sum
        self.cells = []
        # self.used_values = set()

        self.to_do = False

    def __repr__(self):
        if self.sum > 0:
            return f"{self.sum}"  # + f"({self.cells[0].x}, {self.cells[0].y}) ({self.cells[-1].x}, {self.cells[-1].y}) - n_cells: {len(self.cells)} - {self.used_values}"
        if self.sum < 0:
            return str(self.initial_value)

        else:
            return "satisfied"

    def add_cell(self, c):
        # if c.value > 0:
        #     self.sum -= c.value
        #     self.used_values.add(c.value)
        # else:
        self.cells.append(c)
        c.add_constraint(self)

    def check(self):
        s = 0
        vals = []
        for c in self.cells:
            if c.value <= 0:
                return False

            vals.append(c.value)
            s += c.value

        return s == self.sum and len(vals) == len(set(vals))

    def apply(self):
        """
        :return: has_changed_something, is_state_ok
        """
        if self.to_do:
            self.to_do = False

            used_values = set([c.value for c in self.cells]) - {0}
            remaining = self.sum - sum(used_values)
            unassigned = [c for c in self.cells if c.value == 0]
            restricted_possibilites = [set() for i in range(len(unassigned))]

            for assignment in possible_assignments(remaining, len(self.cells) - len(used_values), used_values):
                good = True
                for v, c in zip(assignment, unassigned):
                    good = good and v in c.possible_values

                if good:
                    for rp, v in zip(restricted_possibilites, assignment):
                        rp.add(v)

            something_changed = False
            for rp, c in zip(restricted_possibilites, unassigned):
                if len(c.possible_values) != len(rp):
                    if len(rp) == 0:  # fail case
                        return False, False
                    if len(rp) == 1:
                        c.value = next(iter(rp))

                    c.possible_values = rp
                    something_changed = True
                    for cst in c.constraints:
                        cst.to_do = True

            return something_changed, True

            # val = possible_values(remaining, len(self.cells) - len(used_values), used_values)

            # something_changed = False
            # for c in self.cells:
            #     if c.value == 0:
            #         # restrict the domain of the cell
            #         changed, ok = c.intersect(val)
            #         # if the domain becomes empty return a value that signal that the current state is invalid
            #         if not ok:
            #             return False, False
            #
            #         if changed:
            #             something_changed = True
            #             for cst in c.constraints:
            #                 cst.to_do = True
            #
            # return something_changed, True

        return False, True


class Board:
    def __init__(self, cells):
        self.cells = cells
        for i in range(len(cells)):
            for j in range(len(cells[i])):
                cells[i][j].y = i
                cells[i][j].x = j

        self.constraints = self.extract_constraints(cells)

    def check(self):
        for cst in self.constraints:
            if not cst.check():
                return False

        return True

    def apply_constraints(self):
        for cst in self.constraints:
            cst.to_do = True

        something_changed = True
        while something_changed:
            something_changed = False
            for cst in self.constraints:
                changed, ok = cst.apply()

                if not ok:
                    return False

                something_changed |= changed

        return True

    @staticmethod
    def extract_constraints(cells):
        constraints = []
        for i in range(len(cells)):
            for j in range(len(cells[i])):
                if cells[i][j].r_sum > 0:
                    cst = SumConstraint(cells[i][j].r_sum)
                    for h in range(j + 1, len(cells[i])):
                        if cells[i][h].value == -1:
                            break
                        else:
                            cst.add_cell(cells[i][h])
                    constraints.append(cst)
                if cells[i][j].d_sum > 0:
                    cst = SumConstraint(cells[i][j].d_sum)
                    for h in range(i + 1, len(cells)):
                        if cells[h][j].value == -1:
                            break
                        else:
                            cst.add_cell(cells[h][j])
                    constraints.append(cst)

        return constraints

    def empty_cells(self):
        res = []
        for line in self.cells:
            for c in line:
                if c.value == 0:
                    res.append(c)

        return res

    def set_cell(self, x, y, v):
        self.cells[y][x].set(v)

    def save_as_html(self, out_path):
        with open("template.html") as f:
            template = Template(f.read())

        with open(out_path, "w") as f:
            f.write(template.render(cells=self.cells))

    def copy(self):
        cells = []
        for line in self.cells:
            cells.append([Cell(c.value, c.r_sum, c.d_sum) for c in line])

        return Board(cells)

    @staticmethod
    def from_string(txt):
        txt = txt.replace(" ", "")

        txt_cells = [line.split("|") for line in txt.split("\n") if line]

        cells = []
        for line in txt_cells:
            cells.append([Cell.from_string(c) for c in line])

        return Board(cells)


def possible_values(tot, n, exclude):
    tmp = [set(x) for x in itertools.combinations([1, 2, 3, 4, 5, 6, 7, 8, 9], n) if
           sum(x) == tot and not (set(x) & exclude)]
    if tmp:
        return set.union(*tmp)
    else:
        return set()


def possible_assignments(tot, n, exclude):
    for x in itertools.permutations({1, 2, 3, 4, 5, 6, 7, 8, 9} - exclude, n):
        if sum(x) == tot:
            yield x


def search_solution(board, max_depth=1000):
    if not board.apply_constraints():
        return None, []

    if max_depth == 0:
        return board, []

    empty_cells = board.empty_cells()

    # if there are no empty cells we solved the game
    if len(empty_cells) == 0:
        return board, []

    # fail-first strategy
    empty_cells = sorted(empty_cells, key=lambda x: len(x.possible_values))

    for c in empty_cells:
        for v in c.possible_values:
            next_state = board.copy()
            next_state.set_cell(c.x, c.y, v)

            res, moves = search_solution(next_state, max_depth - 1)
            if res is not None:
                return res, [board] + moves

    return None, []


# for x in possible_assignments(11, 2, {9, 7}):
#     print(x)
for i in range(1, 14):
    with open(f"puzzles/kakuro_{i}.txt") as f:
        board = Board.from_string(f.read())

    s = time.time()
    board, moves = search_solution(board, 20)
    print(board.check())
    e = time.time()

    if board and board.check():
        print(f"Solved problem {i} in {e-s} seconds")
    else:
        print(f"Failed to solve problem {i}")
    #
# # # print(board, moves)
# # board.save_as_html("board.html")
# # board.apply_constraints()
# # board.save_as_html("board_1.html")
# #
# # #
# for i, move in enumerate(moves):
#     move.save_as_html("board_%d.html" % i)
#
# board.save_as_html("board.html")

# print(moves[8].constraints[37])
# print("\n".join(f"{i} {cst}" for i,cst in enumerate(moves[8].constraints)))
# print(moves)
# # print("\n".join(str(cst) for cst in board.constraints))
# board.save_as_html("board.html")
